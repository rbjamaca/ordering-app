
# Ordering App

Ordering app using Laravel and Reactjs

## API Endpoints for Backend
* api/auth/login -> authenticates users and returns json
* api/auth/signup -> registers users
* api/auth/logout -> clears access tokens

## Setup

1. RUN 'composer install' in command line
2. RUN 'php artisan passport:install'
3. RUN 'php artisan migrate'

