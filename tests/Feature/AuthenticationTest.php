<?php

namespace Tests\Feature;


use Illuminate\Support\Str;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;

class AuthenticationTest extends TestCase
{
    private $user;
    private $postInfo;

    use RefreshDatabase;
    
    protected function setUp(): void{

        parent::setUp();
        \Artisan::call('passport:install');

        $this->user = factory(User::class)->create();

        $this->postInfo = [
            'name' => $this->user->name, 
            'email' => 'test@test.com', 
            'password' => 'password', 
            'password_confirmation' => 'password'
        ];
    }

    public function testSignUpWithCorrectDetailsReturnsOkWithCredentials()
    {

        $response = $this->post('/api/auth/signup', $this->postInfo);
        $this->assertDatabaseHas('users', [
            'email' => 'test@test.com'
        ]);
        $response
            ->assertSuccessful()
            ->assertJson(['success' => true]);
    }

    public function testSignUpWithIncorrectDetailsReturnsError()
    {

        $response = $this->post('/api/auth/signup');
        $response->assertSessionHasErrors(['name', 'email', 'password']);
    }

    public function testSignUpWithIncorrectPasswordConfirmationReturnsError()
    {
        $this->withoutExceptionHandling();

        $signUpInfo = [
            'name' => $this->user->name, 
            'email' => 'test@test.com', 
            'password' => 'password', 
            'password_confirmation' => ''
        ];
        try{
            $response = $this->post('/api/auth/signup', $signUpInfo);
        }catch(ValidationException $e){
            // dd($e->validator->errors());
            $this->assertEquals(
                'The password confirmation does not match.',
                $e->validator->errors()->first('password')
            );
            return;
        }
        $this->fail('The mismatched password passed validation when it should have failed.');
    }

    public function testLoginWithCorrectDetailsReturnsOkWithCredentials()
    {
        $loginInfo = ['email' => $this->user->email, 'password' => 'password', 'remember_me' => true];

        $response = $this->post('/api/auth/login', $loginInfo);

        $response
            ->assertSuccessful()
            ->assertJson(['success' => true]);
        
        $data = $response->decodeResponseJson();
        // dd($data['email'], $this->user->email);
        $this->assertEquals($data['email'], $this->user->email);
        $this->assertNotEmpty($data['access_token']);
    }

    public function testSignupWithExistingEmailReturnsAnError()
    {
        $this->withoutExceptionHandling();

        $response = $this->post('/api/auth/signup', $this->postInfo);

        try{
            $response = $this->post('/api/auth/signup', $this->postInfo);
        }catch(ValidationException $e){
            $this->assertEquals(
                'The email has already been taken.',
                $e->validator->errors()->first('email')
            );
            return;
        }
        $this->fail('The email passed validation when it should have failed.');
    }
}
