<?php

namespace Tests;

use Illuminate\Contracts\Console\Kernel;

trait CreatesApplication
{
    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();
        if ( ! $app->hasBeenBootstrapped())
        {
            $app->bootstrapWith(
                [
                    'Illuminate\Foundation\Bootstrap\DetectEnvironment',
                    'Illuminate\Foundation\Bootstrap\LoadConfiguration',
                    'Illuminate\Foundation\Bootstrap\RegisterFacades',
                    'Illuminate\Foundation\Bootstrap\SetRequestForConsole',
                    'Illuminate\Foundation\Bootstrap\RegisterProviders',
                    'Illuminate\Foundation\Bootstrap\BootProviders',
                ]
            );
        }

        return $app;
    }
}
